<?php

namespace Drupal\Tests\onix_codelists_client\Unit;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\onix_codelists_client\OnixCodeListsClient;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Tests the basic functionality of the module.
 *
 * @group onix_codelists_client
 */
class ClientTest extends UnitTestCase {

  /**
   * Http client.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $httpClient;

  /**
   * Cache.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $cache;

  /**
   * The logger.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->httpClient = $this->prophesize(ClientInterface::class);
    $this->cache = $this->prophesize(CacheBackendInterface::class);
    $logger = $this->prophesize(LoggerInterface::class);
    $this->logger = $this->prophesize(LoggerChannelFactoryInterface::class);
    $this->logger->get('onix_codelists_client')->willReturn($logger->reveal());
  }

  /**
   * Test that the client are actually working and returning things.
   */
  public function testClientEmptyResponse() {
    $response = $this->prophesize(ResponseInterface::class);
    $response->getBody()->willReturn('[]');
    $this->httpClient->request('GET', 'https://onix-codelists.io/api/v1/codelist/123?include=codes')->willReturn($response->reveal());
    $onix_client = new OnixCodeListsClient($this->httpClient->reveal(), $this->cache->reveal(), $this->logger->reveal());
    $this->expectExceptionMessage('No JSON data found in onix codelists response');
    $onix_client->getCodelistData(123);
  }

  /**
   * Test some sample data.
   */
  public function testClient() {
    $response = $this->prophesize(ResponseInterface::class);
    $response->getBody()->willReturn('["test", "test2"]');
    $this->httpClient->request('GET', 'https://onix-codelists.io/api/v1/codelist/123?include=codes')->willReturn($response->reveal());
    $onix_client = new OnixCodeListsClient($this->httpClient->reveal(), $this->cache->reveal(), $this->logger->reveal());
    $data = $onix_client->getCodelistData(123);
    $this->assertArrayEquals([
      'test',
      'test2',
    ], $data);
  }

}
