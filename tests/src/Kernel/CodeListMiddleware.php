<?php

namespace Drupal\Tests\onix_codelists_client\Kernel;

use Drupal\Core\State\StateInterface;
use Psr\Http\Message\RequestInterface;

/**
 * Make sure we can count the requests we have done.
 */
class CodeListMiddleware {

  const STATE_KEY = 'codelists_middleware_state_key';

  /**
   * State.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * CodeListMiddleware constructor.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function __invoke() {
    return function ($handler) {
      return function (RequestInterface $request, array $options) use ($handler) {
        $values = $this->state->get(self::STATE_KEY);
        $values[] = $request;
        $this->state->set(self::STATE_KEY, $values);
        return $handler($request, $options);
      };
    };
  }

}
