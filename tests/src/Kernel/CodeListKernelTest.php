<?php

namespace Drupal\Tests\onix_codelists_client\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\onix_codelists_client\OnixCodeListsClient;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Test that the client is returning what we want, and cache and so on.
 *
 * @group onix_codelists_client
 */
class CodeListKernelTest extends KernelTestBase implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Remove this since it creates an error.
    // @see https://www.drupal.org/project/drupal/issues/2571475
    $container->removeDefinition('test.http_client.middleware');
    $container
      ->register('onix_codelists_client.http_client.middleware', CodeListMiddleware::class)
      ->addArgument(new Reference('state'))
      ->addTag('http_client_middleware');
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'onix_codelists_client',
  ];

  /**
   * Test an actual response to the service.
   */
  public function testClientResponse() {
    $this->requestAndAssertKnownCode();
  }

  /**
   * Test the different scenarios where a response should be cached.
   */
  public function testCachedResponse() {
    $state = $this->container->get('state');
    $this->assertNull($state->get(CodeListMiddleware::STATE_KEY));
    $this->requestAndAssertKnownCode();
    $this->assertEqual(count($state->get(CodeListMiddleware::STATE_KEY)), 1);
    // Do it again, it should come fram the cache.
    $this->requestAndAssertKnownCode();
    $this->assertEqual(count($state->get(CodeListMiddleware::STATE_KEY)), 1);
    // Now bypass the cache.
    /** @var \Drupal\onix_codelists_client\OnixCodeListsClient $client */
    $client = $this->container->get('onix_codelists_client.client');
    $data = $client->getCodelistData(169, TRUE, FALSE);
    $this->assertKnownResponse($data);
    $this->assertEqual(count($state->get(CodeListMiddleware::STATE_KEY)), 2);
    // And it should not use cache if the cache tag was cleared.
    $this->container->get('cache_tags.invalidator')->invalidateTags([OnixCodeListsClient::CACHE_TAG]);
    $this->requestAndAssertKnownCode();
    $this->assertEqual(count($state->get(CodeListMiddleware::STATE_KEY)), 3);
  }

  /**
   * Helper.
   */
  protected function requestAndAssertKnownCode() {
    /** @var \Drupal\onix_codelists_client\OnixCodeListsClient $client */
    $client = $this->container->get('onix_codelists_client.client');
    $data = $client->getCodelistData(169);
    $this->assertKnownResponse($data);
  }

  /**
   * Helper.
   */
  protected function assertKnownResponse($data) {
    $this->assertEqual($data->data->description, 'Quantity unit');
  }

}
